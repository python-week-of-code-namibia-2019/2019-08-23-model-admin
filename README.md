# Python Week Of Code, Namibia 2019

[blog/models.py](blog/models.py) has the source code of the database model.

## Task for Instructor

1. Add

   ```
   from django.contrib import admin
   ```

   and

   ```
   urlpatterns = [
       path('', include('blog.urls')),
       path('admin/', admin.site.urls),
   ]
   ```

   to [mysite/urls.py](mysite/urls.py).
2. Open http://localhost:8000/admin/ with the web browser.
3. From PowerShell, run

   ```
   python manage.py createsuperuser
   ```
4. Open http://localhost:8000/admin/ with the web browser.
   And log in using the super user account created in the last step.
5. Add

   ```
   from .models import Post

   admin.site.register(Post)
   ```

   to [blog/admin.py](blog/admin.py) for make the model available in the administrative papel.

## Tasks for Learners

1. Create two posts using the administrative panel.

## Extra

1. Replace

   ```
   admin.site.register(Post)
   ```

   with

   ```
   @admin.register(Post)
   class PostAdmin(admin.ModelAdmin):
       fieldsets = (
           (None, {
               'fields': ('title', 'text')
               }),
           ('Advanced options', {
               'classes': ('collapse',),
               'fields': ('created_date', 'published_date'),
           }),
       )
   ```

   at [blog/admin.py](blog/admin.py).